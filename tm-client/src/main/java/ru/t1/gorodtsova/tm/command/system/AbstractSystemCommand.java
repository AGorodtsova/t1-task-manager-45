package ru.t1.gorodtsova.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.gorodtsova.tm.api.service.ICommandService;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;
import ru.t1.gorodtsova.tm.command.AbstractCommand;
import ru.t1.gorodtsova.tm.enumerated.Role;

public abstract class AbstractSystemCommand extends AbstractCommand {

    @NotNull
    protected ICommandService getCommandService() {
        return serviceLocator.getCommandService();
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return serviceLocator.getPropertyService();
    }

    @Nullable
    @Override
    public Role[] getRoles() {
        return null;
    }

}
