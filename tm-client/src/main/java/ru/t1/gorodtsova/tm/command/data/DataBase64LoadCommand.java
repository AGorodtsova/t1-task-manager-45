package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.domain.DataBase64LoadRequest;

public final class DataBase64LoadCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Load data from base64 file";

    @NotNull
    public static final String NAME = "data-load-base64";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 LOAD]");
        @NotNull final DataBase64LoadRequest request = new DataBase64LoadRequest(getToken());
        getDomainEndpoint().loadDataBase64(request);
    }

}
