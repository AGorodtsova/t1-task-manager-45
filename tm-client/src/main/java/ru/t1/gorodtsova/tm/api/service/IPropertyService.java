package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.component.ISaltProvider;
import ru.t1.gorodtsova.tm.api.endpoint.IConnectionProvider;

public interface IPropertyService extends IConnectionProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

}
