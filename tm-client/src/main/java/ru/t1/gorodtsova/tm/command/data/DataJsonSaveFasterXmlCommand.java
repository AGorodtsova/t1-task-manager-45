package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.domain.DataJsonSaveFasterXmlRequest;

public final class DataJsonSaveFasterXmlCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data in json file";

    @NotNull
    private final String NAME = "data-save-json";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA SAVE JSON]");
        @NotNull final DataJsonSaveFasterXmlRequest request = new DataJsonSaveFasterXmlRequest(getToken());
        getDomainEndpoint().saveDataJsonFasterXml(request);
    }

}
