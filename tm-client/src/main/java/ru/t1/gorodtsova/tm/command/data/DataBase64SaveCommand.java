package ru.t1.gorodtsova.tm.command.data;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.dto.request.domain.DataBase64SaveRequest;

public final class DataBase64SaveCommand extends AbstractDataCommand {

    @NotNull
    private final String DESCRIPTION = "Save data to base64 file";

    @NotNull
    private final String NAME = "data-save-base64";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public void execute() {
        System.out.println("[DATA BASE64 SAVE]");
        @NotNull final DataBase64SaveRequest request = new DataBase64SaveRequest(getToken());
        getDomainEndpoint().saveDataBase64(request);
    }

}
