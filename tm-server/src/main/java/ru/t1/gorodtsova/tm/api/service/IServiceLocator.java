package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.service.dto.*;

public interface IServiceLocator {

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectDtoService getProjectService();

    @NotNull
    IProjectTaskDtoService getProjectTaskService();

    @NotNull
    ITaskDtoService getTaskService();

    @NotNull
    IUserDtoService getUserService();

    @NotNull
    IAuthService getAuthService();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    IDomainService getDomainService();

    @NotNull
    ISessionDtoService getSessionService();

}
