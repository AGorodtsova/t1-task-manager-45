package ru.t1.gorodtsova.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.component.ISaltProvider;

public interface IPropertyService extends ISaltProvider {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getAuthorEmail();

    @NotNull
    String getAuthorName();

    @NotNull
    Integer getServerPort();

    @NotNull
    String getServerHost();

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

    @NotNull
    String getDatabaseUser();

    @NotNull
    String getDatabasePassword();

    @NotNull
    String getDatabaseUrl();

    @NotNull
    String getDatabaseDriver();

    @NotNull
    String getDatabaseDialect();

    @NotNull
    String getDatabaseDdlAuto();

    @NotNull
    String getDatabaseShowSql();

    @NotNull
    String getDatabaseSecondLevelCash();

    @NotNull
    String getDatabaseFactoryClass();

    @NotNull
    String getDatabaseUseQueryCash();

    @NotNull
    String getDatabaseUseMinPuts();

    @NotNull
    String getDatabaseRegionPrefix();

    @NotNull
    String getDatabaseConfigFilePath();

}
