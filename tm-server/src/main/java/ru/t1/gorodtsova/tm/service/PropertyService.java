package ru.t1.gorodtsova.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.gorodtsova.tm.api.service.IPropertyService;

import java.util.Properties;

public final class PropertyService implements IPropertyService {

    @NotNull
    public static final String FILE_NAME = "application.properties";

    @NotNull
    private static final String APPLICATION_VERSION_KEY = "buildNumber";

    @NotNull
    private static final String AUTHOR_EMAIL_KEY = "email";

    @NotNull
    private static final String AUTHOR_NAME_KEY = "developer";

    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "25546";

    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";

    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "356884515";

    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";

    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";

    @NotNull
    private static final String SERVER_PORT_DEFAULT = "6060";

    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";

    @NotNull
    private static final String SERVER_HOST_DEFAULT = "0.0.0.0";

    @NotNull
    private static final String SESSION_KEY = "session.key";

    @NotNull
    private static final String SESSION_TIMEOUT_KEY = "session.timeout";

    @NotNull
    private static final String SESSION_TIMEOUT_DEFAULT = "10800";

    @NotNull
    private static final String DB_USER = "database.username";

    @NotNull
    private static final String DB_USER_DEFAULT = "postgres";

    @NotNull
    private static final String DB_PASSWORD = "database.password";

    @NotNull
    private static final String DB_PASSWORD_DEFAULT = "admin";

    @NotNull
    private static final String DB_URL = "database.url";

    @NotNull
    private static final String DB_DRIVER = "database.driver";

    @NotNull
    private static final String DB_DIALECT = "database.dialect";

    @NotNull
    private static final String DB_DDL_AUTO = "database.hbm2ddl_auto";

    @NotNull
    private static final String DB_SHOW_SQL = "database.show_sql";

    @NotNull
    private static final String DATABASE_SECOND_LEVEL_CASH = "database.second_lvl_cash";

    @NotNull
    private static final String DATABASE_FACTORY_CLASS = "database.factory_class";

    @NotNull
    private static final String DATABASE_USE_QUERY_CASH = "database.use_query_cash";

    @NotNull
    private static final String DATABASE_USE_MIN_PUTS = "database.use_min_puts";

    @NotNull
    private static final String DATABASE_REGION_PREFIX = "database.region_prefix";

    @NotNull
    private static final String DATABASE_CONFIG_FILE_PATH = "database.config_file_path";

    @NotNull
    private static final String EMPTY_VALUE = "---";

    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        properties.load(ClassLoader.getSystemResourceAsStream(FILE_NAME));
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        return Manifests.read(APPLICATION_VERSION_KEY);
    }

    @NotNull
    @Override
    public String getAuthorEmail() {
        return Manifests.read(AUTHOR_EMAIL_KEY);
    }

    @NotNull
    @Override
    public String getAuthorName() {
        return Manifests.read(AUTHOR_NAME_KEY);
    }

    @NotNull
    private String getEnvKey(@NotNull final String key) {
        return key.replace(".", "_").toUpperCase();
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        return getIntegerValue(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT);
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        return getStringValue(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getServerPort() {
        return getIntegerValue(SERVER_PORT_KEY, SERVER_PORT_DEFAULT);
    }

    @NotNull
    @Override
    public String getServerHost() {
        return getStringValue(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @NotNull
    @Override
    public String getSessionKey() {
        return getStringValue(SESSION_KEY);
    }

    @NotNull
    @Override
    public Integer getSessionTimeout() {
        return getIntegerValue(SESSION_TIMEOUT_KEY, SESSION_TIMEOUT_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUser() {
        return getStringValue(DB_USER, DB_USER_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabasePassword() {
        return getStringValue(DB_PASSWORD, DB_PASSWORD_DEFAULT);
    }

    @NotNull
    @Override
    public String getDatabaseUrl() {
        return getStringValue(DB_URL);
    }

    @NotNull
    @Override
    public String getDatabaseDriver() {
        return getStringValue(DB_DRIVER);
    }

    @NotNull
    @Override
    public String getDatabaseDialect() {
        return getStringValue(DB_DIALECT);
    }

    @NotNull
    @Override
    public String getDatabaseDdlAuto() {
        return getStringValue(DB_DDL_AUTO);
    }

    @NotNull
    @Override
    public String getDatabaseShowSql() {
        return getStringValue(DB_SHOW_SQL);
    }

    @Override
    public @NotNull String getDatabaseSecondLevelCash() {
        return getStringValue(DATABASE_SECOND_LEVEL_CASH);
    }

    @Override
    public @NotNull String getDatabaseFactoryClass() {
        return getStringValue(DATABASE_FACTORY_CLASS);
    }

    @Override
    public @NotNull String getDatabaseUseQueryCash() {
        return getStringValue(DATABASE_USE_QUERY_CASH);
    }

    @Override
    public @NotNull String getDatabaseUseMinPuts() {
        return getStringValue(DATABASE_USE_MIN_PUTS);
    }

    @Override
    public @NotNull String getDatabaseRegionPrefix() {
        return getStringValue(DATABASE_REGION_PREFIX);
    }

    @Override
    public @NotNull String getDatabaseConfigFilePath() {
        return getStringValue(DATABASE_CONFIG_FILE_PATH);
    }

    @NotNull
    private Integer getIntegerValue(@NotNull final String key, @NotNull final String defaultValue) {
        return Integer.parseInt(getStringValue(key, defaultValue));
    }

    @NotNull
    private String getStringValue(@NotNull final String key, @NotNull final String defaultValue) {
        if (System.getProperties().contains(key)) return System.getProperties().getProperty(key);
        @NotNull final String envKey = getEnvKey(key);
        if (System.getenv().containsKey(envKey)) return System.getenv(envKey);
        return properties.getProperty(key, defaultValue);
    }

    @NotNull
    private String getStringValue(@NotNull final String key) {
        return getStringValue(key, EMPTY_VALUE);
    }

}
